加密解密
===========
# 环境要求
1. php>=7.2
2. openssl扩展

# 开始使用
``
composer require zhujinkui/encryption
``
+ 简单使用
````
<?php
require_once __DIR__ . '/../vendor/autoload.php';

$encryption = new \think\Encryption();
//加密
$result = $encryption->encode('d12347477');
var_dump($result->getRes());
//解密
$dres = $encryption->key($result->getKey())->iv($result->getIv())->decode($result->getRes());
var_dump($dres->getRes());
````
# LICENSE 
[MIT]('./LICENSE')
